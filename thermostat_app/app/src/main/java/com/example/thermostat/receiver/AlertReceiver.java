package com.example.thermostat.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.example.thermostat.HomePageActivity;

import static com.example.thermostat.StaticItems.thermostat;

public class AlertReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean setOpen = intent.getBooleanExtra("OPEN", false);
        int iconVisibility = View.GONE;
        thermostat.setOpen(setOpen);
        Long tsLong = System.currentTimeMillis();
        if(setOpen) {
            int temp = intent.getIntExtra("TEMPERATURE", 25);
            int mode = intent.getIntExtra("MODE", 0 );
            thermostat.setWorkingTemperature(temp);
            thermostat.setMode(mode);
            iconVisibility = View.VISIBLE;
        }
        HomePageActivity homePageActivity = new HomePageActivity();
        homePageActivity.loadHomePage();
        homePageActivity.schedulerOnIcon.setVisibility(iconVisibility);
    }
}


