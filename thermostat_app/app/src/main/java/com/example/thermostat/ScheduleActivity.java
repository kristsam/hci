package com.example.thermostat;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.thermostat.domain.ScheduleItem;
import com.example.thermostat.receiver.AlertReceiver;

import java.util.ArrayList;
import java.util.List;

import static com.example.thermostat.StaticItems.schedule;
import static com.example.thermostat.StaticItems.thermostatText;

public class ScheduleActivity extends AppCompatActivity implements Adapter.OnItemListener, DialogInterface.OnDismissListener, OnDeleteItemAlertDialog.OnDeleteListener , OnActivateCollidedScheduleAlertDialog.OnActivateListener {

    TextView scheduleView;
    RecyclerView recyclerView;
    Button addButton, editButton, deleteButton;
    ImageButton backToHomeButton;
    GeneralScheduleDialog addDialog, editDialog;
    OnDeleteItemAlertDialog deleteDialog;
    OnActivateCollidedScheduleAlertDialog activateDialog;
    Adapter myAdapter;
    ScheduleItem mscheduleItem;
    Intent intentToHomePage;
    List<Integer> collidedIndexes;
    int switchPosition;

    int previous = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        intentToHomePage = new Intent();
        collidedIndexes = new ArrayList<Integer>();

        recyclerView = findViewById(R.id.recycler);
        scheduleView = findViewById(R.id.scheduleView);

        backToHomeButton = (ImageButton) findViewById(R.id.backToHomeButton2);

        addButton = (Button) findViewById(R.id.addButton);
        editButton = (Button) findViewById(R.id.editButton);
        deleteButton = (Button) findViewById(R.id.deleteButton);
        setEnabledButtons(false);

        initRecycler();
        loadSchedule();

    }

    @Override
    protected void onStart(){
        super.onStart();
        addButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view)
            {
                addDialog = new AddScheduleDialog(ScheduleActivity.this);
                addDialog.setOnDismissListener(ScheduleActivity.this);
                addDialog.show();
            }
        });

        editButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view)
            {
                editDialog = new EditScheduleDialog(ScheduleActivity.this, mscheduleItem);
                editDialog.setOnDismissListener(ScheduleActivity.this);
                editDialog.show();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view)
            {
                deleteDialog = new OnDeleteItemAlertDialog();
                deleteDialog.show(getSupportFragmentManager(), "example");
            }
        });

        backToHomeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view)
            {
                onHomeButtonPressed();
            }
        });

    }

    protected void onHomeButtonPressed(){
        setResult(Activity.RESULT_FIRST_USER, intentToHomePage);
        finish();
    }

    protected void loadSchedule(){
        changeTextSize(thermostatText.getTextSize());
    }

    private void changeTextSize(int textSize) {
        float f_textSize = (float)textSize;
        scheduleView.setTextSize(TypedValue.COMPLEX_UNIT_SP,(float)1.2*f_textSize);
    }

    public void updateAdapter() {
        myAdapter.notifyDataSetChanged();
    }

    //call this method from adapter listener when a schedule item is selected(true when selects, false when unselects)
    public void setEnabledButtons(boolean enabled){
        editButton.setEnabled(enabled);
        deleteButton.setEnabled(enabled);
    }

    private void initRecycler() {
        myAdapter = new Adapter(this, schedule,this);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onItemClick(int position) {

        if(position==previous) {
            setEnabledButtons(false);
            previous = -1;
            myAdapter.adjustColor(position, Adapter.UNCLICK);
        } else {
            setEnabledButtons(true);
            previous = position;
            myAdapter.adjustColor(position, Adapter.CLICK);
            myAdapter.adjustRestColors(position);
        }

        mscheduleItem = schedule.get(position);
    }

    @Override
    public void onEnable(boolean enable, ScheduleItem item) {
        mscheduleItem = item;
        if(enable){
            switchPosition = myAdapter.getItemPosition(item);
            collidedIndexes = myAdapter.getCollidedSchedulesIndexes(switchPosition);
            if(!collidedIndexes.isEmpty()) {
                activateDialog = new OnActivateCollidedScheduleAlertDialog();
                activateDialog.setCancelable(false);
                activateDialog.show(getSupportFragmentManager(), "example");
            }else{
                editAlarm(PendingIntent.FLAG_UPDATE_CURRENT);
            }
        }else{
            editAlarm(PendingIntent.FLAG_CANCEL_CURRENT);
            HomePageActivity.schedulerOnIcon.setVisibility(View.GONE);
        }
    }

    private void editAlarm(int flags) {
//        FIXME TODO day doesn't work
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        setAlarm(alarmManager,  flags, false, mscheduleItem);
        setAlarm(alarmManager,  flags,true, mscheduleItem);
    }

    @Override
    public void onDismiss(DialogInterface dialogInterface) {
        updateAdapter();
        int flags = 0;
        if(dialogInterface.getClass() == EditScheduleDialog.class ) {
            flags = PendingIntent.FLAG_UPDATE_CURRENT;
        }else if(dialogInterface.getClass() == AddScheduleDialog.class){
            mscheduleItem = schedule.get(schedule.size()-1);
        }
        if(mscheduleItem.isActivated()) {
            editAlarm(flags);
        }
    }

    private void setAlarm(AlarmManager alarmManager,int flags, boolean open, ScheduleItem scheduleItem){
        int requestCode = scheduleItem.getFromRequestCode();
        long timeInMillis = scheduleItem.getFromDate().getTimeInMillis();
        if (!open){
            requestCode = scheduleItem.getToRequestCode();
            timeInMillis = scheduleItem.getToDate().getTimeInMillis();
        }
        Intent intent = new Intent(this, AlertReceiver.class);
        intent.putExtra("OPEN",open);
        intent.putExtra("TEMPERATURE",scheduleItem.getTemperature());
        intent.putExtra("MODE" ,scheduleItem.getMode());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, requestCode, intent, flags);
        if(flags == PendingIntent.FLAG_CANCEL_CURRENT) {
            alarmManager.cancel(pendingIntent);
        }else{
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, timeInMillis, pendingIntent);
        }
    }

    @Override
    public void onDelete() {
        ScheduleItem delItem = mscheduleItem;
        schedule.remove(delItem);
        setEnabledButtons(false);
        myAdapter.adjustRestColors(-1);
        updateAdapter();
        editAlarm(PendingIntent.FLAG_CANCEL_CURRENT);
    }

    @Override
    public void onActivate() {
        myAdapter.disableCollidedSchedules(collidedIndexes);
        editAlarm(PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onNotActivate() {
        myAdapter.disableSchedule(switchPosition);
        editAlarm(PendingIntent.FLAG_CANCEL_CURRENT);
    }

}