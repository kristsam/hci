package com.example.thermostat.domain;

import java.text.DateFormatSymbols;
import java.util.Calendar;

import static com.example.thermostat.StaticItems.request_code;

public class ScheduleItem {

    private int temperature;
    private boolean isActivated;
    private Calendar fromDate, toDate;
    private int requestCode, mode;

    public ScheduleItem(){
        fromDate = Calendar.getInstance();
        toDate = Calendar.getInstance();
        fromDate.set(Calendar.SECOND, 0);
        toDate.set(Calendar.SECOND, 0);
        fromDate.setFirstDayOfWeek(Calendar.MONDAY);
        toDate.setFirstDayOfWeek(Calendar.MONDAY);
        isActivated = false;
        requestCode = ++request_code;
        mode=0;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public Calendar getToDate() {
        return toDate;
    }

    public void setToDate(Calendar toDate) {
        this.toDate = toDate;
    }

    public Calendar getFromDate() {
        return fromDate;
    }

    public void setFromDate(Calendar date) {
        this.fromDate = date;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getFromDay() {
        return fromDate.get(Calendar.DAY_OF_WEEK);
    }

    public void setFromDay(int fromDay) {
        this.fromDate.set(Calendar.DAY_OF_WEEK,fromDay);
    }

    public int getToDay() {
        return toDate.get(Calendar.DAY_OF_WEEK);
    }

    public void setToDay(int toDay) {
        this.toDate.set(Calendar.DAY_OF_WEEK,toDay);
    }

    public int getFromHour() {
        return fromDate.get(Calendar.HOUR_OF_DAY);
    }

    public void setFromHour(int fromHour) {
        this.fromDate.set(Calendar.HOUR_OF_DAY, fromHour);
    }

    public int getToHour() {
        return toDate.get(Calendar.HOUR_OF_DAY);
    }

    public void setToHour(int toHour) {
        this.toDate.set(Calendar.HOUR_OF_DAY, toHour);
    }

    public int getFromMin() {
        return fromDate.get(Calendar.MINUTE);
    }

    public void setFromMin(int fromMin) {
        this.fromDate.set(Calendar.MINUTE, fromMin);
    }

    public int getToMin() {
        return toDate.get(Calendar.MINUTE);
    }

    public void setToMin(int toMin) {
        this.toDate.set(Calendar.MINUTE, toMin);
    }

    public boolean isActivated() {
        return isActivated;
    }

    public void setActivated(boolean activated) {
        isActivated = activated;
    }

    public int getFromRequestCode() {
        return requestCode;
    }

    public int getToRequestCode(){
        return -getFromRequestCode();
    }

    public String getFromDayToString(){
        String[] days = DateFormatSymbols.getInstance().getWeekdays();
        return days[getFromDay()].toUpperCase();
    }

    public String getToDayToString(){
        String[] days = DateFormatSymbols.getInstance().getWeekdays();
        return days[getToDay()].toUpperCase();
    }

}
