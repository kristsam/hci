package com.example.thermostat.domain;

import java.util.Calendar;

public class ThermostatText {

    int text_size;
    final int [] TEXT_SIZE = {16,22,28};

    public ThermostatText(){
        text_size=TEXT_SIZE[1];
    }

    public int getTextSize(){
        return text_size;
    }

    public void setTextSize(int size){
        text_size = size;
    }

    public void setTextOffsetSize(int b) {
        if ( b<3 && b>-1){
            text_size = TEXT_SIZE[b];
        }
    }

    public int getTextOffsetSize(){
        if (text_size == TEXT_SIZE[0]) return 0;
        if (text_size == TEXT_SIZE[1]) return 1;
        if (text_size == TEXT_SIZE[2]) return 2;
        return -1;
    }

    public int getDayFromString(String text){
        int day;
        if(text.equals("MONDAY")){
            day = Calendar.MONDAY;
        }else if(text.equals("TUESDAY")){
            day = Calendar.TUESDAY;
        }else if(text.equals("WEDNESDAY")){
            day = Calendar.WEDNESDAY;
        }else if(text.equals("THURSDAY")){
            day = Calendar.THURSDAY;
        }else if(text.equals("FRIDAY")){
            day = Calendar.FRIDAY;
        }else if(text.equals("SATURDAY")){
            day = Calendar.SATURDAY;
        }else{
            day = Calendar.SUNDAY;
        }
        return day;
    }

    public int getNextDay(int day){
        if(day >= Calendar.SATURDAY || day <= 0){
            return Calendar.SUNDAY;
        }
        return (day + 1);
    }

}
