package com.example.thermostat;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.thermostat.domain.ScheduleItem;
import com.google.android.material.button.MaterialButtonToggleGroup;

import java.text.DateFormatSymbols;
import java.util.Calendar;

import static com.example.thermostat.StaticItems.thermostat;
import static com.example.thermostat.StaticItems.thermostatText;

abstract class GeneralScheduleDialog extends Dialog implements PopupMenu.OnMenuItemClickListener{

    protected TextView addScheduleText, titleText, tempMetricView, saveFailed;
    protected PopupMenu popupMenu;
    protected Button startDayMenu, endDayMenu, saveButton, cancelButton, currentButton;
    protected TimePicker startTimePicker, endTimePicker;
    protected NumberPicker tempPicker;
    protected ScheduleItem scheduleItem;
    protected MaterialButtonToggleGroup modeUnitGroup;
    protected Button autoButton, coldButton, hotButton;

    public GeneralScheduleDialog(@NonNull Context context) {
        super(context);
    }

    public GeneralScheduleDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected GeneralScheduleDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addScheduleText = (TextView) findViewById(R.id.text);
        titleText = (TextView) findViewById(R.id.addEditScheduleLabel);
        tempMetricView = (TextView) findViewById(R.id.addScheduleMetricView);
        saveFailed = (TextView) findViewById(R.id.saveFailed);
        saveFailed.setVisibility(View.GONE);

        startTimePicker = (TimePicker) findViewById(R.id.startTimePicker);
        endTimePicker = (TimePicker) findViewById(R.id.endTimePicker);

        tempPicker = (NumberPicker) findViewById(R.id.tempPicker);
        int minTemp = thermostat.getMin();
        int maxTemp = thermostat.getMax();
        tempPicker.setMaxValue(maxTemp);
        tempPicker.setMinValue(minTemp);

        startDayMenu = (Button) findViewById(R.id.startDayButton);
        endDayMenu = (Button) findViewById(R.id.endDayButton);

        modeUnitGroup = (MaterialButtonToggleGroup)findViewById(R.id.modeUnitGroup);

        autoButton = (Button)findViewById(R.id.autoChooseButton);
        coldButton = (Button)findViewById(R.id.coldChooseButton);
        hotButton = (Button)findViewById(R.id.hotChooseButton);


        saveButton = (Button) findViewById(R.id.saveButton);
        cancelButton = (Button) findViewById(R.id.cancelButton);

        loadScheduleDialog();

    }

    protected void loadScheduleDialog() {
        startTimePicker.setIs24HourView(true);
        endTimePicker.setIs24HourView(true);
        tempMetricView.setText(thermostat.temperatureUnitToString());
        changeTextSize(thermostatText.getTextSize());
    }

    @Override
    protected void onStart() {
        super.onStart();
        startDayMenu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view)
            {
                popupMenu = new PopupMenu(getContext(), view);
                currentButton = startDayMenu;
                String [] days = createWeekDays();
                loadPopup(days);
                showPopup();
            }
        });
        endDayMenu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view)
            {
                popupMenu = new PopupMenu(getContext(), view);
                currentButton = endDayMenu;
                String [] days = createWeekDays();
                loadPopup(days);
                showPopup();
            }
        });
        modeUnitGroup.addOnButtonCheckedListener(new MaterialButtonToggleGroup.OnButtonCheckedListener() {
            @Override
            public void onButtonChecked(MaterialButtonToggleGroup group, int checkedId, boolean isChecked) {
                if (isChecked) {
                    if (checkedId == autoButton.getId()) {
                        scheduleItem.setMode(0);
                    }else if(checkedId == coldButton.getId()){
                        scheduleItem.setMode(1);
                    }else if(checkedId == hotButton.getId()){
                        scheduleItem.setMode(2);
                    }
                }else{
                    if(modeUnitGroup.getCheckedButtonId() == -1){
                        modeUnitGroup.check(checkedId);
                    }
                }
            }
        });
        saveButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view)
            {
                onSaveButtonPressed();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view)
            {
                onCancelButtonPressed();
            }
        });
    }

//    specializes call of save
    abstract protected void endSave(ScheduleItem scheduleItem);

    protected void onCancelButtonPressed(){
//        NOTE when cancel pressed, don't make event
        this.setOnDismissListener(null);
        this.dismiss();
    }

    public void show(){
        super.show();
    }

    public void loadPopup(String[] input){

        for(String s : input){
            popupMenu.getMenu().add(s);
        }

    }

    protected void onSaveButtonPressed(){

        if(!startDayMenu.getText().toString().trim().endsWith("..")) {

            scheduleItem.setFromDay(thermostatText.getDayFromString((String)startDayMenu.getText()));
            scheduleItem.setFromHour(startTimePicker.getHour());
            scheduleItem.setFromMin(startTimePicker.getMinute());
//            if user doesn't choose end day it will be considered as the same with se start day
            if(endDayMenu.getText().toString().trim().endsWith("..")){
                scheduleItem.setToDay(thermostatText.getDayFromString((String)startDayMenu.getText()));
                /*if(endTimePicker.getHour() < startTimePicker.getHour()){
                    scheduleItem.setToDay(thermostatText.getNextDay(thermostatText.getDayFromString((String)startDayMenu.getText())));
                }*/
            }else{
                scheduleItem.setToDay(thermostatText.getDayFromString((String)endDayMenu.getText()));
            }
            scheduleItem.setToHour(endTimePicker.getHour());
            scheduleItem.setToMin(endTimePicker.getMinute());
            scheduleItem.setTemperature(tempPicker.getValue());
            if(scheduleItem.getFromDay() == scheduleItem.getToDay()
                    && (scheduleItem.getFromHour() > scheduleItem.getToHour() ||
                    (scheduleItem.getFromHour() == scheduleItem.getToHour() &&
                            scheduleItem.getFromMin() > scheduleItem.getToMin()))){
                scheduleItem.getToDate().set(Calendar.DAY_OF_YEAR, scheduleItem.getFromDate().get(Calendar.DAY_OF_YEAR) + 7);
            }

            endSave(scheduleItem);

            this.dismiss();
        }else{
            saveFailed.setVisibility(View.VISIBLE);
        }
    }

    private String[] createWeekDays(){
        String[] old = DateFormatSymbols.getInstance().getWeekdays();
        String [] days = new String[old.length-1];
//        SUNDAY LAST
        for (int i = 1; i < old.length-1; i++) {
            days[i-1] = old[i+1].toUpperCase();
        }
        days[days.length-1] = old[1].toUpperCase();
        return days;
    }

    abstract protected void showPopup();

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return changeCurrentButtonText((String)item.getTitle());
    }

    protected boolean changeCurrentButtonText(String value){
        currentButton.setText(value);
        currentButton = null;
        return true;
    }

    private void changeTextSize(int textSize) {
        float f_textSize = (float)textSize;
        titleText.setTextSize(TypedValue.COMPLEX_UNIT_SP,(float)1.2*f_textSize);
        saveFailed.setTextSize(TypedValue.COMPLEX_UNIT_SP,(float)1.2*f_textSize);
    }


}
