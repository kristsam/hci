package com.example.thermostat;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import java.util.List;

public class OnActivateCollidedScheduleAlertDialog extends AppCompatDialogFragment {

    final String DIALOG_MESSAGE = "There is another activated schedule that collides on date with this one. " +
            "Do you wish to deactivate the other one and activate this one instead?";
    String answer;
    androidx.appcompat.app.AlertDialog.Builder builder;
    private OnActivateListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setMessage(this.DIALOG_MESSAGE).setPositiveButton("Yes", this.getDialogClickListener())
                .setNegativeButton("No", this.getDialogClickListener());
        return builder.create();
    }

    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    answer = "Y";
                    listener.onActivate();
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    answer = "N";
                    listener.onNotActivate();
                    break;
            }
        }
    };

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try{
            listener = (OnActivateListener) context;
        }catch (ClassCastException e){
            throw new ClassCastException();
        }
    }

    public DialogInterface.OnClickListener getDialogClickListener(){
        return dialogClickListener;
    }

    /*public void setCanceledOnTouchOutside(boolean b) {
    }*/

    public interface OnActivateListener{
        void onActivate();
        void onNotActivate();
    }

}
