package com.example.thermostat;

import com.example.thermostat.domain.ScheduleItem;
import com.example.thermostat.domain.Thermostat;
import com.example.thermostat.domain.ThermostatText;

import java.util.ArrayList;
import java.util.List;

public final class StaticItems {

    //    includes all the thermostat info variables in order to read and change
    public static Thermostat thermostat = new Thermostat();

    //    includes all text info variables in order to read and change
    public static ThermostatText thermostatText = new ThermostatText();

    //    includes schedule info
    public static List<ScheduleItem> schedule = new ArrayList<ScheduleItem>();

    //    unique request code for alarm events
    public static int request_code = 0;

}
