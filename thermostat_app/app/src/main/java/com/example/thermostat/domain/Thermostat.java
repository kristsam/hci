package com.example.thermostat.domain;

import static com.example.thermostat.StaticItems.schedule;

public class Thermostat {

    final int MAX_TEMP_C = 40;
    final int MIN_TEMP_C = 0;

//    declares if thermostat is currently working
    boolean isOpen;

//    declares what mode is currently working 0=auto, 1=cold, 2=heat
    int mode;

//  temperature unit, false=Celsius, true=Fahrenheit
    boolean isFahrenheit;

//  currently working temperature, -100 stands for not working
    int working_temperature;

    int outside_world_temperature;

    String city;

    int brightness;

    boolean systemPromise;

    ScheduleItem current_schedule;

    public Thermostat(){
        isOpen = true;
        mode = 0;
        isFahrenheit = false;
        working_temperature = 25;
        outside_world_temperature = 32;
        city = "Athens";
        brightness = 127;
        systemPromise = false;
        current_schedule = null;
    }

    public ScheduleItem getCurrentSchedule() {
        if(current_schedule != null)
            throw new NullPointerException("No current scheduler!");
        return current_schedule;
    }

    public void setCurrentSchedule(ScheduleItem current_schedule) {
        this.current_schedule = current_schedule;
    }

    public boolean isSystemPromise() {
        return systemPromise;
    }

    public void setSystemPromise(boolean systemPromise) {
        this.systemPromise = systemPromise;
    }

    public int getBrightness() {
        return brightness;
    }

    public void setBrightness(int brightness) {
        this.brightness = brightness;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

//    changes Open state to the opposite
    public void autoChangeOpen(){
        isOpen = !isOpen;
        if(!isOpen) working_temperature = -100;
        if(isOpen){
            working_temperature = 25;
            if (isFahrenheit)
                working_temperature = fromCelsiusToFahrenheit(working_temperature);
        }
    }

//    goes to the next program mode
    public void nextMode(){
        mode = (mode +1)%3;
    }

    public int getMode(){
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public void setToCelsius() {
        if (!isFahrenheit) return;
        isFahrenheit = false;
        if(isOpen) {
            working_temperature = fromFahrenheitToCelsius(working_temperature);
        }
        for(ScheduleItem item: schedule){
            int temp = item.getTemperature();
            item.setTemperature(fromFahrenheitToCelsius(temp));
        }
        outside_world_temperature = fromFahrenheitToCelsius(outside_world_temperature);
    }

    public void setToFahrenheit() {
        if (isFahrenheit) return;
        isFahrenheit = true;
        if(isOpen) {
            working_temperature = fromCelsiusToFahrenheit(working_temperature);
        }
        for(ScheduleItem item: schedule){
            int temp = item.getTemperature();
            item.setTemperature(fromCelsiusToFahrenheit(temp));
        }
        outside_world_temperature = fromCelsiusToFahrenheit(outside_world_temperature);
    }

    public boolean isFahrenheit(){
        return isFahrenheit;
    }

    public int getWorkingTemperature(){
        return working_temperature;
    }

    public void setWorkingTemperature(int working_temperature){
        this.working_temperature = working_temperature;
    }

    public int getOutsideWorldTemperature(){
        return outside_world_temperature;
    }

    public int getMin() {
        if(!isFahrenheit()) return MIN_TEMP_C;
        return fromCelsiusToFahrenheit(MIN_TEMP_C);
    }

    public int getMax() {
        if(!isFahrenheit()) return MAX_TEMP_C;
        return fromCelsiusToFahrenheit(MAX_TEMP_C);
    }

    public void onTemperatureDown() {
        if (working_temperature > getMin()) {
            working_temperature--;
        }
    }

    public void onTemperatureUp() {
        if(working_temperature < getMax()){
            working_temperature++;
        }
    }

    public String temperatureUnitToString(){
        if(isFahrenheit) return "F";
        return "C";
    }

    public void setOutsideWorldTemperature(int outside_world_temperature) {
        this.outside_world_temperature = outside_world_temperature;
    }

    private int fromCelsiusToFahrenheit(int num){
        return (int) Math.round(1.8 * num + 32);
    }

    private int fromFahrenheitToCelsius(int num){
        return (int)Math.round((num - 32)/1.8);
    }
}
