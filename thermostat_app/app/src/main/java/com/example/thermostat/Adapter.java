package com.example.thermostat;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.thermostat.domain.ScheduleItem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.example.thermostat.StaticItems.schedule;
import static com.example.thermostat.StaticItems.thermostat;
import static com.example.thermostat.StaticItems.thermostatText;

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {

    List<ScheduleItem> data;
    Context context;
    int selectedItem;
    OnItemListener mOnItemListener;
    List<MyViewHolder> holders;
    int position;
    static final String CLICK = "click";
    static final String UNCLICK = "unclick";

    public Adapter(Context ct, List<ScheduleItem> schedule,OnItemListener onItemListener){
        context = ct;
        data = schedule;
        selectedItem = -1;
        this.mOnItemListener = onItemListener;
        holders = new ArrayList<MyViewHolder>();
        this.position = -1;
    }

    @NonNull
    @Override
    public Adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.new_row,parent,false);
        return new  MyViewHolder(view,mOnItemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter.MyViewHolder holder, int position) {

        if (!holders.contains(holder)) {
            holder.daysText1.setTextColor(Color.parseColor("#000000"));
            holder.hoursText.setTextColor(Color.parseColor("#000000"));
            holder.tempText.setTextColor(Color.parseColor("#000000"));
            holder.metricText.setTextColor(Color.parseColor("#000000"));
            holders.add(holder);
        }

        ScheduleItem item = data.get(position);
        String dayFrom = item.getFromDayToString();
        String dayTo = item.getToDayToString();
        String day;
        if(dayTo.equals(dayFrom)){
            day = dayFrom;
        }else{
            day = dayFrom+" - "+dayTo;
        }

        String hourFrom = String.valueOf(item.getFromHour());
        String minFrom = String.valueOf(item.getFromMin());
        String hourTo = String.valueOf(item.getToHour());
        String minTo = String.valueOf(item.getToMin());

        minFrom = addZeroIfNeeded(minFrom);
        minTo = addZeroIfNeeded(minTo);
        hourFrom = addZeroIfNeeded(hourFrom);
        hourTo = addZeroIfNeeded(hourTo);

//        display style
        String time = hourFrom+" : "+minFrom+" - "+hourTo+" : "+minTo;

        String temp = String.valueOf(item.getTemperature());

        String metric = thermostat.temperatureUnitToString();

        holder.daysText1.setText(day);
        holder.hoursText.setText(time);
        holder.tempText.setText(temp);
        holder.metricText.setText(metric);
        holder.activatedSwitch.setChecked(item.isActivated());
//        mode
        int mode = item.getMode();
        if(mode == 0) {
            holder.modeView.setImageResource(R.mipmap.auto2);
        }else if(mode == 1) {
            holder.modeView.setImageResource(R.mipmap.snow2);
        }else{
            holder.modeView.setImageResource(R.mipmap.hot2);
        }
//        changes text size due to settings, weight in text size
        int text_size = thermostatText.getTextSize();
        holder.daysText1.setTextSize(TypedValue.COMPLEX_UNIT_SP,(float) (text_size*0.9));
        holder.hoursText.setTextSize(TypedValue.COMPLEX_UNIT_SP,(float) (text_size*0.9));
        holder.tempText.setTextSize(TypedValue.COMPLEX_UNIT_SP,(float) (text_size*0.9));
        holder.metricText.setTextSize(TypedValue.COMPLEX_UNIT_SP,(float) (text_size*0.45));

        holder.activatedSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                data.get(position).setActivated(isChecked);
                mOnItemListener.onEnable(isChecked, data.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView daysText1, hoursText, tempText, metricText;
        Switch activatedSwitch;
        OnItemListener onItemListener;
        CardView cardView;
        ImageView modeView;

        public MyViewHolder(@NonNull View itemView,OnItemListener onItemListener) {
            super(itemView);
            daysText1 = itemView.findViewById(R.id.dayText);
            hoursText = itemView.findViewById(R.id.hourText);
            tempText = itemView.findViewById(R.id.tempText);
            metricText = itemView.findViewById(R.id.rowMetricText);
            cardView = itemView.findViewById(R.id.cardView);
            activatedSwitch = itemView.findViewById(R.id.activatedSwitch);
            activatedSwitch.setChecked(false);
            modeView = itemView.findViewById(R.id.modeListView);
            this.onItemListener = onItemListener;
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            onItemListener.onItemClick(getAdapterPosition());
        }
    }

    public void removeItem(ScheduleItem scheduleItem){
        schedule.remove(scheduleItem);
    }

    public interface OnItemListener {
        void onItemClick(int position) ;
//        enables item if true or else disables
        void onEnable(boolean enable, ScheduleItem item);
    }

    public MyViewHolder getCurrentHolder(int position){
        return this.holders.get(position);
    }

    public void adjustRestColors(int position){
        for(int i = 0; i < this.holders.size(); i++){
            if(i != position){
                this.holders.get(i).cardView.setBackgroundColor(Color.parseColor("#FFFFFF"));
                this.holders.get(i).daysText1.setTypeface(null, Typeface.NORMAL);
                this.holders.get(i).hoursText.setTypeface(null, Typeface.NORMAL);
                this.holders.get(i).tempText.setTypeface(null, Typeface.NORMAL);
                this.holders.get(i).metricText.setTypeface(null, Typeface.NORMAL);
            }
        }
    }

    public void adjustColor(int position, String state){
        if(state.equals(CLICK)){
            this.holders.get(position).cardView.setBackgroundColor(Color.parseColor("#d3b4fa"));
            this.holders.get(position).daysText1.setTypeface(null, Typeface.BOLD);
            this.holders.get(position).hoursText.setTypeface(null, Typeface.BOLD);
            this.holders.get(position).tempText.setTypeface(null, Typeface.BOLD);
            this.holders.get(position).metricText.setTypeface(null, Typeface.BOLD);

        }else if(state.equals(UNCLICK)){
            this.holders.get(position).cardView.setBackgroundColor(Color.parseColor("#FFFFFF"));
            this.holders.get(position).daysText1.setTypeface(null, Typeface.NORMAL);
            this.holders.get(position).hoursText.setTypeface(null, Typeface.NORMAL);
            this.holders.get(position).tempText.setTypeface(null, Typeface.NORMAL);
            this.holders.get(position).metricText.setTypeface(null, Typeface.NORMAL);
        }
    }

    public void disableCollidedSchedules(List<Integer> indexes){
        for(Integer position : indexes){
            disableSchedule(position);
        }
    }

    public void disableSchedule(int position){
        this.holders.get(position).activatedSwitch.setChecked(false);
        data.get(position).setActivated(false);
    }

    public List<Integer> getCollidedSchedulesIndexes(int position){
        List<Integer> indexes = new ArrayList<Integer>();
        ScheduleItem thisItem = data.get(position);
        Calendar thisFromDate = thisItem.getFromDate();
        Calendar thisToDate = thisItem.getToDate();
        for(int i = 0; i < this.data.size(); i++){
            boolean dateCollision = false;
            if(i != position) {
                dateCollision = dateCollides(thisFromDate, data.get(i).getFromDate(), thisToDate, data.get(i).getToDate());
            }
            if(i != position && dateCollision && data.get(i).isActivated()){
                //this.holders.get(i).activatedSwitch.setOnCheckedChangeListener(null);
                //this.holders.get(i).activatedSwitch.setChecked(false);
                indexes.add(i);
                //data.get(i).setActivated(false);
                //this.holders.get(i).activatedSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener());
            }
        }
        return indexes;
    }

    private boolean dateCollides(Calendar fromDate1, Calendar fromDate2, Calendar toDate1, Calendar toDate2){
        boolean case1 = fromDate1.compareTo(fromDate2) <= 0 && fromDate2.compareTo(toDate1) <= 0;
        boolean case2 = fromDate1.compareTo(toDate2) <= 0 && toDate2.compareTo(toDate1) <= 0;
        boolean case3 = fromDate1.compareTo(fromDate2) <= 0 && toDate2.compareTo(toDate1) <= 0;
        boolean case4 = fromDate1.compareTo(fromDate2) >= 0 && toDate2.compareTo(toDate1) >= 0;
        boolean collision = case1 || case2 || case3 || case4;
        return collision;
    }

    public int getItemPosition(ScheduleItem item){
        return data.indexOf(item);
    }

    private String addZeroIfNeeded(String value){
        if (value.length()==1) return "0"+value;
        return value;
    }

}
