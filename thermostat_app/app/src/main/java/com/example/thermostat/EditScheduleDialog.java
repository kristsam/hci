package com.example.thermostat;

import android.content.Context;
import android.os.Bundle;

import com.example.thermostat.domain.ScheduleItem;

class EditScheduleDialog extends GeneralScheduleDialog {


    public EditScheduleDialog(final Context context, ScheduleItem scheduleItem)
    {
        // Set your theme here
        super(context);

        // This is the layout XML file that describes your Dialog layout
        this.setContentView(R.layout.popup_add_schedule);
        this.scheduleItem = scheduleItem;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onStart(){
        super.onStart();
    }

    protected void endSave(ScheduleItem scheduleItem){

//        ScheduleActivity sa = new ScheduleActivity();
//        sa.updateAdapter();
    }

    protected void showPopup(){
        popupMenu.setOnMenuItemClickListener(EditScheduleDialog.this);
        popupMenu.inflate(R.menu.popup_menu);
        popupMenu.show();
    }

    @Override
    protected void loadScheduleDialog() {
        super.loadScheduleDialog();
        titleText.setText("EDIT");
        if(this.scheduleItem != null) {
            startDayMenu.setText(this.scheduleItem.getFromDayToString());
            endDayMenu.setText(this.scheduleItem.getToDayToString());
            startTimePicker.setHour(this.scheduleItem.getFromHour());
            startTimePicker.setMinute(this.scheduleItem.getFromMin());
            endTimePicker.setHour(this.scheduleItem.getToHour());
            endTimePicker.setMinute(this.scheduleItem.getToMin());
            tempPicker.setValue(this.scheduleItem.getTemperature());
        }
    }

}
