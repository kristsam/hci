package com.example.thermostat;


import android.content.Context;

import android.os.Bundle;

import com.example.thermostat.domain.ScheduleItem;

import static com.example.thermostat.StaticItems.schedule;

class AddScheduleDialog extends GeneralScheduleDialog{

    public AddScheduleDialog(final Context context)
    {
        // Set your theme here
        super(context);

        // This is the layout XML file that describes your Dialog layout
        this.setContentView(R.layout.popup_add_schedule);
        this.scheduleItem = new ScheduleItem();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onStart(){
        super.onStart();
    }

    protected void endSave(ScheduleItem scheduleItem){
        scheduleItem.setActivated(false);
        schedule.add(scheduleItem);
//        ScheduleActivity sa = new ScheduleActivity();
//        sa.updateAdapter();
    }

    protected void showPopup(){
        popupMenu.setOnMenuItemClickListener(AddScheduleDialog.this);
        popupMenu.inflate(R.menu.popup_menu);
        popupMenu.show();
    }

    @Override
    protected void loadScheduleDialog() {
        super.loadScheduleDialog();
//        set first value to be the median
        tempPicker.setValue((tempPicker.getMaxValue()+tempPicker.getMinValue())/2);
        titleText.setText("ADD");
    }
}