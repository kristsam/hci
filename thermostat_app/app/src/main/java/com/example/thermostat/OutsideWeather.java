package com.example.thermostat;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import static com.example.thermostat.HomePageActivity.secondaryIndicator;
import static com.example.thermostat.StaticItems.thermostat;

public class OutsideWeather extends AsyncTask<String, Void, String[]> {
    private final String LOG_TAG = OutsideWeather.class.getSimpleName();

    private String[] getWeatherDataFromJson(String forecastJsonStr) throws JSONException {
        // These are the names of the JSON objects that need to be extracted.
        final String OWM_TEMPERATURE = "temp";
        final String OWM_DESCRIPTION = "main";

        JSONObject forecastJson = new JSONObject(forecastJsonStr);
        JSONObject mainJson = forecastJson.getJSONObject(OWM_DESCRIPTION);

        String[]  resultStrs = new String[1];

            // Get the JSON object representing the day
        double temperature = mainJson.getDouble(OWM_TEMPERATURE);
        resultStrs[0] = String.valueOf(temperature);
        return resultStrs;
    }

    @Override
    protected String[] doInBackground(String... params) {
        // These two need to be declared outside the try/catch
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        if (params.length == 0) {
            return null;
        }

        // Will contain the raw JSON response as a string.
        String forecastJsonStr = null;

        String unit = "metric";
        if(thermostat.isFahrenheit()) unit = "imperial";

        try {
            // Construct the URL for the OpenWeatherMap query
            // Possible parameters are avaiable at OWM's forecast API page, at
            // http://openweathermap.org/API#current
            String baseUrl = "https://api.openweathermap.org/data/2.5/weather?id=264371&units="+unit;
            // Here insert your unique APPID
            String apiKey = "&APPID=c088ea57ae056ec383ee37f61ca9dfd0";
            URL url = new URL(baseUrl.concat(apiKey));

            // Create the request to OpenWeatherMap, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
             InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }
            forecastJsonStr = buffer.toString();
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attemping
            // to parse it.
            return null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }

        try {
            return getWeatherDataFromJson(forecastJsonStr);
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String[] results) {
        super.onPostExecute(results);
        try {
            double dResult = Double.parseDouble(results[0]);
            thermostat.setOutsideWorldTemperature((int) dResult);
        }catch(NumberFormatException e){
            thermostat.setOutsideWorldTemperature(0);
        }
        secondaryIndicator.setText(thermostat.getOutsideWorldTemperature()+"");
    }
}
