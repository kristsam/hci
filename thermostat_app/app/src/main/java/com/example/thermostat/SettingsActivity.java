package com.example.thermostat;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.material.button.MaterialButtonToggleGroup;

import static com.example.thermostat.StaticItems.thermostat;
import static com.example.thermostat.StaticItems.thermostatText;

public class SettingsActivity extends AppCompatActivity {

    MaterialButtonToggleGroup temperatureUnitGroup, fontSizeGroup;
    SeekBar brightnessBar;
    ImageButton backToHomeButton;
    TextView settingsText, brightnessText, fontSizeText, temperatureUnitText;
    Button celsiusButton, fahrenheitButton, smallFontButton, mediumFontButton, largeFontButton;
    Intent intentToHomePage, thisIntent;
    ContentResolver cResolver;
    Window window;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        intentToHomePage = new Intent();
        thisIntent = getIntent();

        cResolver = getContentResolver();
        window = getWindow();

        temperatureUnitGroup = (MaterialButtonToggleGroup)findViewById(R.id.temperatureUnitGroup);
        fontSizeGroup = (MaterialButtonToggleGroup)findViewById(R.id.fontSizeUnitGroup);

        celsiusButton = (Button)findViewById(R.id.celsiusButton);
        fahrenheitButton = (Button)findViewById(R.id.fahrenheitButton);
        smallFontButton = (Button)findViewById(R.id.fontSizeSmallButton);
        mediumFontButton = (Button)findViewById(R.id.fontSizeMediumButton);
        largeFontButton = (Button)findViewById(R.id.fontSizeLargeButton);

        brightnessBar = (SeekBar)findViewById(R.id.brightnessSeekBar);
        brightnessBar.setMax(255);

        backToHomeButton = (ImageButton)findViewById(R.id.backToHomeButton2);

        settingsText = (TextView)findViewById(R.id.settingsLabel);
        brightnessText = (TextView)findViewById(R.id.brightnessLabel);
        fontSizeText = (TextView)findViewById(R.id.fontSizeLabel);
        temperatureUnitText = (TextView)findViewById(R.id.unitLabel);

        loadSettings();

    }

    @Override
    protected void onStart(){
        super.onStart();
        brightnessBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
            {
                if(Settings.System.canWrite(getApplicationContext())) thermostat.setSystemPromise(true);
                if(!thermostat.isSystemPromise()) {
                    requestSystemPermission();
                }else{
                    thermostat.setBrightness(progress);
                    onBrightnessUpdated();
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //TO-DO
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //TO-DO
            }
        });
        backToHomeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view)
            {
                onHomeButtonPressed();
            }
        });
        temperatureUnitGroup.addOnButtonCheckedListener(new MaterialButtonToggleGroup.OnButtonCheckedListener() {
            @Override
            public void onButtonChecked(MaterialButtonToggleGroup group, int checkedId, boolean isChecked) {
                if (isChecked) {
                    if (checkedId == celsiusButton.getId()) {
                        thermostat.setToCelsius();
                    }else if(checkedId == fahrenheitButton.getId()){
                        thermostat.setToFahrenheit();
                    }
                }else{
                    if(temperatureUnitGroup.getCheckedButtonId() == -1){
                        temperatureUnitGroup.check(checkedId);
                    }
                }
            }
        });
        fontSizeGroup.addOnButtonCheckedListener(new MaterialButtonToggleGroup.OnButtonCheckedListener() {
            @Override
            public void onButtonChecked(MaterialButtonToggleGroup group, int checkedId, boolean isChecked) {
                if (isChecked) {
                    if (checkedId == smallFontButton.getId()) {
                        thermostatText.setTextOffsetSize(0);
                    }else if(checkedId == mediumFontButton.getId()){
                        thermostatText.setTextOffsetSize(1);
                    }else if(checkedId == largeFontButton.getId()){
                        thermostatText.setTextOffsetSize(2);
                    }
                    changeTextSize(thermostatText.getTextSize());
                }else{
                    if(fontSizeGroup.getCheckedButtonId() == -1){
                        fontSizeGroup.check(checkedId);
                    }
                }
            }
        });
    }

    protected void onHomeButtonPressed(){
        setResult(Activity.RESULT_FIRST_USER, intentToHomePage);
        finish();
    }

    protected void loadSettings(){//read the current unit configuration from thermostat & thermostatText objects
        String unitState = thermostat.temperatureUnitToString();
        if(unitState.equals("C")){
            temperatureUnitGroup.check(celsiusButton.getId());
        }else if (unitState.equals("F")){
            temperatureUnitGroup.check(fahrenheitButton.getId());
        }

        int textSize = thermostatText.getTextOffsetSize();
        if(textSize == 0){
            fontSizeGroup.check(smallFontButton.getId());
        }else if (textSize == 1){
            fontSizeGroup.check(mediumFontButton.getId());
        }else if (textSize ==2 ){
            fontSizeGroup.check(largeFontButton.getId());
        }
        changeTextSize(thermostatText.getTextSize());
        boolean systemPromise = Settings.System.canWrite(getApplicationContext());
        if(!systemPromise) {
            loadDefaultBrightness();
        }else{
            loadSystemBrightness();
        }
        thermostat.setSystemPromise(systemPromise);
    }

    private void changeTextSize(int textSize) {
        float f_textSize = (float)textSize;
        settingsText.setTextSize(TypedValue.COMPLEX_UNIT_SP,(float)1.2*f_textSize);
        brightnessText.setTextSize(TypedValue.COMPLEX_UNIT_SP,f_textSize);
        fontSizeText.setTextSize(TypedValue.COMPLEX_UNIT_SP,f_textSize);
        temperatureUnitText.setTextSize(TypedValue.COMPLEX_UNIT_SP,f_textSize);
    }

    private void requestSystemPermission(){
        Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS)
                .setData(Uri.parse("package:" + this.getPackageName()))
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void loadDefaultBrightness(){
        brightnessBar.setProgress(thermostat.getBrightness());
    }

    private void loadSystemBrightness(){
        try
        {
            // To handle the auto
            Settings.System.putInt(cResolver,
                    Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
            //Get the current system brightness
            int brightness = Settings.System.getInt(cResolver, Settings.System.SCREEN_BRIGHTNESS);
            thermostat.setBrightness(brightness);
            brightnessBar.setProgress(brightness);
        }
        catch (Settings.SettingNotFoundException e)
        {
            //Throw an error case it couldn't be retrieved
            Log.e("Error", "Cannot access system brightness");
            e.printStackTrace();
        }
    }

    private void onBrightnessUpdated(){
        //Set the system brightness using the brightness variable value
        Settings.System.putInt(cResolver, Settings.System.SCREEN_BRIGHTNESS, thermostat.getBrightness());
        //Get the current window attributes
        WindowManager.LayoutParams layoutpars = window.getAttributes();
        //Set the brightness of this window
        layoutpars.screenBrightness = thermostat.getBrightness() / (float)255;
        //Apply attribute changes to this window
        window.setAttributes(layoutpars);
    }

}