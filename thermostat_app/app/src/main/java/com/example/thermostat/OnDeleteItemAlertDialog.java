package com.example.thermostat;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

public class OnDeleteItemAlertDialog extends AppCompatDialogFragment {

    final String DIALOG_MESSAGE = "Are you sure that you want to delete this item?";
    String answer;
    androidx.appcompat.app.AlertDialog.Builder builder;
    private OnDeleteListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setMessage(this.DIALOG_MESSAGE).setPositiveButton("Yes", this.getDialogClickListener())
                .setNegativeButton("No", this.getDialogClickListener());
        return builder.create();
    }


    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    answer = "Y";
                    listener.onDelete();
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    answer = "N";
                    break;
            }
        }
    };

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try{
        listener = (OnDeleteListener) context;
        }catch (ClassCastException e){
            throw new ClassCastException();
        }
    }

    public DialogInterface.OnClickListener getDialogClickListener(){
        return dialogClickListener;
    }

    public interface OnDeleteListener{
        void onDelete();
    }

}
