package com.example.thermostat;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import static com.example.thermostat.StaticItems.thermostat;
import static com.example.thermostat.StaticItems.thermostatText;

public class HomePageActivity extends AppCompatActivity {

    public static ImageButton scheduleBut, settingsBut, arrowTopBut, arrowBottomBut, powerBut, modeBut;
    public static TextView mainUnitIndicator, secondaryUnitIndicator, mainIndicator, secondaryIndicator, cityIndicator, disabledIndicator;//main is the big middle one, secondary is the outside one
    public static ImageView schedulerOnIcon;

    private final static int INTERVAL = 1000 * 60 * 10; //10 minutes
    Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_page_activity);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        scheduleBut = findViewById(R.id.scheduleButton);
        settingsBut = findViewById(R.id.settingsButton);
        modeBut = findViewById(R.id.modeButton);

        arrowTopBut = (ImageButton)findViewById(R.id.arrowTopButton);
        arrowBottomBut = (ImageButton)findViewById(R.id.arrowBottomButton);

        powerBut = (ImageButton)findViewById(R.id.powerButton);

        mainIndicator = (TextView)findViewById(R.id.temperatureView);
        secondaryIndicator = (TextView)findViewById(R.id.outsideTemperatureView);

        mainUnitIndicator = (TextView)findViewById(R.id.temperatureMetricView2);
        secondaryUnitIndicator = (TextView)findViewById(R.id.temperatureMetricView);

        cityIndicator = (TextView) findViewById(R.id.cityView);
        disabledIndicator = (TextView) findViewById(R.id.disabledView);

        schedulerOnIcon = (ImageView) findViewById(R.id.schedulerOn);
        schedulerOnIcon.setVisibility(View.GONE);

        startRepeatingTask();


        loadHomePage();


        scheduleBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_schedule = new Intent(HomePageActivity.this,ScheduleActivity.class);
                startActivity(intent_schedule);
            }
        });

        settingsBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_settings = new Intent(HomePageActivity.this,SettingsActivity.class);
                startActivityForResult(intent_settings, Activity.RESULT_FIRST_USER);
            }
        });

        arrowTopBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onTemperatureUp();
            }
        });

        arrowBottomBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onTemperatureDown();
            }
        });

        powerBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                thermostat.autoChangeOpen();
                setTempOpenUI(thermostat.isOpen());
            }
        });

        modeBut.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                thermostat.nextMode();
                changeModeUI(thermostat.getMode());
            }
        });

    }

    Runnable mHandlerTask = new Runnable()
    {
        @Override
        public void run() {
            OutsideWeather outsideWeather = new OutsideWeather();
            outsideWeather.execute("0020");
            mHandler.postDelayed(mHandlerTask, INTERVAL);
        }
    };

    void startRepeatingTask()
    {
        mHandlerTask.run();
    }

    void stopRepeatingTask()
    {
        mHandler.removeCallbacks(mHandlerTask);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopRepeatingTask();
    }

    private void changeModeUI(int newMode) {

        if(newMode == 0){
            modeBut.setImageResource(R.mipmap.auto2);
        }else if(newMode == 1){
            modeBut.setImageResource(R.mipmap.snow2);
        }else{
            modeBut.setImageResource(R.mipmap.hot2);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent){//handles request to return to home page
        if(resultCode == Activity.RESULT_FIRST_USER){
            loadHomePage();
        }
        super.onActivityResult(requestCode, resultCode, intent);
    }

    public void loadHomePage(){//handles the settings changes
        mainUnitIndicator.setText(thermostat.temperatureUnitToString());

//        TODO take automatically city
        secondaryUnitIndicator.setText(thermostat.temperatureUnitToString());
        cityIndicator.setText(thermostat.getCity());
        setTempOpenUI(thermostat.isOpen());
        secondaryIndicator.setText(thermostat.getOutsideWorldTemperature() + "");
        changeModeUI(thermostat.getMode());
        changeTextSize(thermostatText.getTextSize());
    }

    private void changeTextSize(int textSize) {
//        declare analogies
        float f_textSize = (float)textSize;
        float metric_temp_ratio = (float) 0.25;
        mainIndicator.setTextSize(TypedValue.COMPLEX_UNIT_SP,6*f_textSize);
        mainUnitIndicator.setTextSize(TypedValue.COMPLEX_UNIT_SP, metric_temp_ratio*6*f_textSize);
        secondaryIndicator.setTextSize(TypedValue.COMPLEX_UNIT_SP,3*f_textSize);
        secondaryUnitIndicator.setTextSize(TypedValue.COMPLEX_UNIT_SP,metric_temp_ratio*3*f_textSize);
        cityIndicator.setTextSize(TypedValue.COMPLEX_UNIT_SP,(float)1*f_textSize);
        disabledIndicator.setTextSize(TypedValue.COMPLEX_UNIT_SP,(float)1.4*f_textSize);
    }

    protected void onTemperatureUp(){
        thermostat.onTemperatureUp();
        mainIndicator.setText(thermostat.getWorkingTemperature() + "");
    }

    protected void onTemperatureDown(){
        thermostat.onTemperatureDown();
        mainIndicator.setText(thermostat.getWorkingTemperature() + "");
    }

//    changes screen UI when thermostat starts/stops working
    protected void setTempOpenUI(boolean becameOpen){
        if(becameOpen){
            disabledIndicator.setVisibility(View.INVISIBLE);
            mainIndicator.setText(thermostat.getWorkingTemperature()+"");
            mainIndicator.setVisibility(View.VISIBLE);
            mainUnitIndicator.setVisibility(View.VISIBLE);
            arrowTopBut.setVisibility(View.VISIBLE);
            arrowBottomBut.setVisibility(View.VISIBLE);
            modeBut.setVisibility(View.VISIBLE);
        }else{
            mainIndicator.setVisibility(View.INVISIBLE);
            disabledIndicator.setVisibility(View.VISIBLE);
            mainUnitIndicator.setVisibility(View.INVISIBLE);
            arrowTopBut.setVisibility(View.INVISIBLE);
            arrowBottomBut.setVisibility(View.INVISIBLE);
            modeBut.setVisibility(View.INVISIBLE);
            schedulerOnIcon.setVisibility(View.INVISIBLE);
        }
    }

}